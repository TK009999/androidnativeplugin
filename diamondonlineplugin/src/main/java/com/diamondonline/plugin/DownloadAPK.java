package com.diamondonline.plugin;

import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.File;

public class DownloadAPK {

    public String urlString, app_name, notification_description;

    private PluginCallback callback;

    public DownloadAPK(String urlString, String app_name, String notification_description) {
        this.urlString = urlString;
        this.app_name = app_name;
        this.notification_description = notification_description;
    }

    public void Initialize(PluginCallback callback) {
        this.callback = callback;
    }

    public void CheckVersionDownload(final Context ctx) {

        String destination = GetFolderPath();
        String fileName = app_name + ".apk";
        destination += fileName;

        final Uri uri = Uri.parse("file://" + destination);
        ;

        File file = new File(destination);
        //Delete update file if exists
        if (file.exists()) file.delete();

        //get url of app on server
        String url = urlString;

        //set downloadmanager
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setDescription(notification_description);
        request.setTitle(app_name);

        //set destination
        request.setDestinationUri(uri);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) ctx.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);

        //set BroadcastReceiver to install app when .apk is downloaded
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context con, Intent intent) {

                Activity activity = (Activity) ctx;

                Intent install = null;

                // 如果 Andorid 大於等於 7.0
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    File file = new File(uri.getPath());

                    try {

                        PackageManager manager = ctx.getPackageManager();

                        PackageInfo info = manager.getPackageInfo(ctx.getPackageName(), 0);

                        Log.d("Plugin Error", info.packageName);

                        Uri apkUri = FileProvider.getUriForFile(activity, info.packageName + ".fileprovider", file);
                        install = new Intent(Intent.ACTION_DEFAULT);
                        install.setDataAndType(apkUri, "application/vnd.android.package-archive");
                        install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                        install.putExtra(Intent.EXTRA_ALLOW_REPLACE, true);
                        install.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                        install.putExtra(Intent.EXTRA_INSTALLER_PACKAGE_NAME, activity.getApplicationInfo().packageName);

                    } catch (PackageManager.NameNotFoundException e) {

                        e.printStackTrace();

                        Log.d("Plugin Error", e.getMessage());
                    }

                } else {

                    install = new Intent(Intent.ACTION_VIEW);
                    install.setDataAndType(uri, "application/vnd.android.package-archive");
                    install.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                    install.putExtra(Intent.EXTRA_ALLOW_REPLACE, true);
                    install.putExtra(Intent.EXTRA_RETURN_RESULT, true);
                    install.putExtra(Intent.EXTRA_INSTALLER_PACKAGE_NAME, activity.getApplicationInfo().packageName);
                }


                Log.d("Plugin Error", "Star New Activity");
                install.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(install);

                Log.d("Plugin Error", "Activity finished");
                activity.finish();

                activity.unregisterReceiver(this);

                if (callback != null)
                    callback.onSuccess("onSuccess");
            }
        };

        //register receiver for when .apk download is compete
        ctx.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    private String GetFolderPath() {

        //if there is no SD card, create new directory objects to make directory on device
        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/DiamondOnline/");

        // if no directory exists, create new directory
        if (!directory.exists()) {
            directory.mkdir();
        }

        String Path = directory.getPath();
        Path += "/";

        return Path;
    }
}
