package com.diamondonline.plugin;

//import needed packages / classes.

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.PermissionChecker;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

public class Plugin extends UnityPlayerActivity {

    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 0;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    // our native method, which will be called from Unity3D
    public void PrintString(final Context ctx, final String message) {
        //create / show an android toast, with message and short duration.
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public boolean CheckOpenPacakage(String packageName) {
        PackageManager packageManager = getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(packageName);
        if (intent != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RequestPermission(final Context ctx) {

        if (!isGranted(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) ctx, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    ActivityCompat.requestPermissions((Activity) ctx,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.REQUEST_INSTALL_PACKAGES},
                            MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                } else {
                    ActivityCompat.requestPermissions((Activity) ctx,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.REQUEST_INSTALL_PACKAGES},
                            MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
                }

            } else {
                ActivityCompat.requestPermissions((Activity) ctx,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.REQUEST_INSTALL_PACKAGES},
                        MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            }

        } else {
            UnityPlayer.UnitySendMessage("LoginBaseView", "OnRequestPermissionSuccessCallback", "onSuccess");
        }
    }

    public void DownloadAPK(final Context ctx, String urlString, String app_name, String notification_description) {
        DownloadAPK downloadAPK = new DownloadAPK(urlString, app_name, notification_description);
        downloadAPK.Initialize(new PluginCallback() {
            @Override
            public void onSuccess(String videoPath) {
                UnityPlayer.UnitySendMessage("LoginBaseView", "OnDownloadFinishedCallback", "onDownloadFinished");
            }
        });
        downloadAPK.CheckVersionDownload(ctx);
    }

    private boolean isGranted(final Context ctx, final String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                PackageManager.PERMISSION_GRANTED == PermissionChecker.checkCallingOrSelfPermission(ctx, permission);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                UnityPlayer.UnitySendMessage("LoginBaseView", "OnRequestPermissionErrorCallback", "onError");
                return;
            }

            UnityPlayer.UnitySendMessage("LoginBaseView", "OnRequestPermissionSuccessCallback", "onSuccess");
            return;
        }

        UnityPlayer.UnitySendMessage("LoginBaseView", "OnRequestPermissionErrorCallback", "onError");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 100) {

            if (resultCode == Activity.RESULT_OK) {
                UnityPlayer.UnitySendMessage("LoginBaseView", "OnActivityResult", "OK");
            } else if (resultCode == Activity.RESULT_CANCELED) {
                UnityPlayer.UnitySendMessage("LoginBaseView", "OnActivityResult", "Canceled");
            } else {
                UnityPlayer.UnitySendMessage("LoginBaseView", "OnActivityResult", "Field");
            }
        }
    }

    public int GetVersionCode(final Context ctx) {
        Context context = ctx;
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        int versionCode = -1;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public String GetVersionName(final Context ctx) {
        Context context = ctx;
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        String versionName = "";
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
