package com.diamondonline.plugin;

public interface PluginCallback {
    public void onSuccess(String videoPath);
}
